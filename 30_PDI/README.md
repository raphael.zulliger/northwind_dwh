# What's inside

These PDI transformations and jobs perform all the ETL to populate the DWH
with data from the ERP.

The usual way to "run ETL" is to open the job file `00_Job.kjb` and run
it inside PDI.

Alternatively, you may execute each transformation manually. Ensure to run
them in the right order, which is, ordered by their number: `10_xxx` before
`20_xxx` etc. Executing the transformations manually usually only makes
sense for debugging purposes.