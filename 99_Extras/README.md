# What's inside

This folder contains some extra stuff, which is not strictly needed to
build and run the DWH:

- `01_play_exec`: An SQL playbook player. It was built from scratch for
  the development of this DWH. It allows to separate the various SQL
  steps to separate `.sql` files and still run them by a simple command
  line invocation like `sqlplay.py 20_create_dwh`
- `20_ExportCSV`: Contains PDI translations that would export data from
  the ERP to `.csv` before importing them into the staging aready of the
  DWH. This has been created to show Dani that we "can" create and load
  `.csv`... but we consider loading the data directly from MySQL as
  superior and therefore used that method in our actual PDI translations.