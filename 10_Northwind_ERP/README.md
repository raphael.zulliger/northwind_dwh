# What's inside

This folder contains all .sql files required to "create" the Northwind ERP
database and "fill it" with data `10_create_db_and_data`. It also contains
some `.sql` files to change data of the ERP, see `30_change_data`. The latter
is useful to test whether the DWH behaves as expected when the ERP data
change over time

- The files which's names start with a number, such as 01_File.sql are the 
  "origin" files, those to be edited if you like to change any of the date
- The "Overall.sql" file is simply an automatically generated "concatenated" 
  version of all the number files. It's not ought to be edit manually

Thus, you can either execute each number file separately or you can run the
"Overall.sql" to achieve the same thing
