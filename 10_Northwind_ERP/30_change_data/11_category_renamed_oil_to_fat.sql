USE `northwind`;

--
-- Change 'Oil' by 'Fat' (which is nonsense of course, but helps to test our DWH ;-))
--
-- The purpose of this script is to cause "some changes" to the data in OLTP to test whether the SCD properly works.
--

UPDATE `products` SET category = 'Fat' WHERE category = 'Oil'
