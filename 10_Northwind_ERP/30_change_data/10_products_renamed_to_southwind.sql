USE `northwind`;

--
-- Change the product_names' 'Northwind' by 'Southwind'
--
-- The purpose of this script is to cause "some changes" to the data in OLTP to test whether the SCD properly works.
--

UPDATE `products` SET product_name = REPLACE(product_name, 'Northwind', 'Southwind') WHERE INSTR(product_name, 'Northwind') > 0;
