USE `northwind`;

--
-- Change the state/province of a customer (he obviously moved location)
--
-- The purpose of this script is to cause "some changes" to the data in OLTP to test whether the SCD properly works.
--

UPDATE `customers` SET `state_province` = 'MA' WHERE `company` = 'Company C';
