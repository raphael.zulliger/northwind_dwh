USE `northwind`;

--
-- Add a new customer
--
-- The purpose of this script is to cause "some changes" to the data in OLTP to test whether the SCD properly works.
--

INSERT INTO `customers` (`id`, `company`, `last_name`, `first_name`, `email_address`, `job_title`, `business_phone`, `home_phone`, `mobile_phone`, `fax_number`, `address`, `city`, `state_province`, `zip_postal_code`, `country_region`, `web_page`, `notes`, `attachments`) VALUES (30, 'Company DD', 'Hansueli', 'Supergeilo', NULL, 'CCCEO', '(123)555-0100', NULL, NULL, '(123)555-0101', '789 29th Street', 'Denver', 'MA', '99999', 'USA', NULL, NULL, '');
