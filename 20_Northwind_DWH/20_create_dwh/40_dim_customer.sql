USE `northwind_dwh`;

CREATE TABLE `dim_customer` (
    id INT NOT NULL,
    version INT NOT NULL,
    date_from DATETIME,
    date_to DATETIME,

    customer_id INT,
    company varchar(50),
    state_province varchar(50),
    city varchar(50),
    PRIMARY KEY(id)
);

CREATE INDEX idx_dim_customer_lookup ON dim_customer(customer_id);
