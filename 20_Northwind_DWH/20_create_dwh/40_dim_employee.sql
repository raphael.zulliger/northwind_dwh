USE `northwind_dwh`;

CREATE TABLE `dim_employee` (
    id INT NOT NULL,
    version INT NOT NULL,
    date_from DATETIME,
    date_to DATETIME,

    employee_id INT,
    full_name varchar(100),
    email_address varchar(50),

    PRIMARY KEY(id)
);

CREATE INDEX idx_dim_employee_lookup ON dim_employee(employee_id);