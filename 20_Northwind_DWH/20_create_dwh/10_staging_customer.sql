USE `northwind_dwh`;

CREATE TABLE `staging_customer` (
    id INT NOT NULL,
    company varchar(50) NOT NULL,
    state_province varchar(50) NOT NULL,
    city varchar(50) NOT NULL,
    
    PRIMARY KEY(id)
);
