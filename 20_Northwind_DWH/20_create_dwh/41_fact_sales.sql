USE `northwind_dwh`;

CREATE TABLE `fact_sales` (
    order_date_id INT NOT NULL,
    shipped_date_id INT NOT NULL,
    paid_date_id INT NOT NULL,

    customer_id INT NOT NULL,
    employee_id INT NOT NULL,
    product_id INT NOT NULL,

    unit_price DECIMAL(19, 4) NOT NULL,
    quantity INT NOT NULL,
    revenue DECIMAL(19, 4) NOT NULL,
    discount DOUBLE NOT NULL DEFAULT 0,

    FOREIGN KEY fk_date(order_date_id) REFERENCES dim_date(id),
    FOREIGN KEY fk_shipped_date(shipped_date_id) REFERENCES dim_date(id),
    FOREIGN KEY fk_paid_date(paid_date_id) REFERENCES dim_date(id),
    FOREIGN KEY fk_customer(customer_id) REFERENCES dim_customer(id),
    FOREIGN KEY fk_employee(employee_id) REFERENCES dim_employee(id),
    FOREIGN KEY fk_product(product_id) REFERENCES dim_product(id),

    PRIMARY KEY(order_date_id, shipped_date_id, paid_date_id, customer_id, employee_id, product_id)
);
