USE `northwind_dwh`;

CREATE TABLE `dim_product` (
    id INT NOT NULL,
    version INT NOT NULL,
    date_from DATETIME,
    date_to DATETIME,

    product_id INT,
    category varchar(50),
    product_name varchar(50),
    standard_cost DECIMAL(19,4),

    PRIMARY KEY(id)
);

CREATE INDEX idx_dim_product_lookup ON dim_product(product_id);
