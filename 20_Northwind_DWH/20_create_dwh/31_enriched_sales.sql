USE `northwind_dwh`;

CREATE TABLE `enriched_sales` (
    order_id INT NOT NULL,
    order_details_id INT NOT NULL,

    order_date DATE NOT NULL,
    shipped_date DATE NOT NULL,
    paid_date DATE NOT NULL,
    employee_id INT NOT NULL,
    customer_id INT NOT NULL,
    product_id INT NOT NULL,

    unit_price DECIMAL(19,4) NOT NULL,
    quantity INT NOT NULL,
    discount DOUBLE NOT NULL DEFAULT 0,
    revenue DECIMAL(19, 4) NOT NULL,    -- a calculated value: (unit_price * quantity) * (1.0 - discount/100)

    PRIMARY KEY(order_id, order_details_id)
);
