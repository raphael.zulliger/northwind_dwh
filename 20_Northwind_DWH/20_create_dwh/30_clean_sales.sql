USE `northwind_dwh`;

CREATE TABLE `clean_sales` (
    order_id INT NOT NULL,
    order_details_id INT NOT NULL,

    order_date DATETIME,
    shipped_date DATETIME,
    paid_date DATETIME,
    employee_id INT NOT NULL,
    customer_id INT NOT NULL,
    product_id INT NOT NULL,

    unit_price DECIMAL(19,4) NOT NULL,
    quantity INT NOT NULL,
    discount DOUBLE NOT NULL DEFAULT 0,

    PRIMARY KEY(order_id, order_details_id)
);
