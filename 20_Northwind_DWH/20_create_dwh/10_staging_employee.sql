USE `northwind_dwh`;

CREATE TABLE `staging_employee` (
    id INT NOT NULL,
    last_name varchar(50) NOT NULL,
    first_name varchar(50) NOT NULL,
    email_address varchar(50) NOT NULL,

    PRIMARY KEY(id)
) ENGINE=InnoDB AUTO_INCREMENT=1000;
