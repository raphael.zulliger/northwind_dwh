USE `northwind_dwh`;

CREATE TABLE `staging_order_details` (
    id INT NOT NULL,
    
    discount DOUBLE NOT NULL DEFAULT 0,
    order_id INT NOT NULL,
    product_id INT NOT NULL,
    quantity INT NOT NULL,
    unit_price DECIMAL(19,4) NOT NULL,

    PRIMARY KEY(id)
);
