USE `northwind_dwh`;

CREATE TABLE `enriched_employee` (
    id INT NOT NULL,
    full_name varchar(100),
    email_address varchar(50),

    PRIMARY KEY(id)
);
