-- This SQL script creates a 'date dimension' table and fills it with data.
-- It does so by using a 'stored procedure', which is also created as part
-- of this script. IOW, this script does:
--   - Create a `dim_date` table
--   - Create a stored procedure to "generate date data"
--   - Calls the stored procedure for a specific date range (e.g. 1.1.2005 to 31.12.2007)

-- from http://tech.akom.net/archives/36-Creating-A-Basic-Date-Dimension-Table-in-MySQL.html

USE `northwind_dwh`;

CREATE TABLE dim_date  (
    id INT NOT NULL auto_increment,
    fulldate date,
    dayofmonth int,
    dayofyear int,
    dayofweek int,
    dayname varchar(10),
    monthnumber int,
    monthname varchar(10),
    year    int,
    quarter tinyint,
    PRIMARY KEY(id)
) ENGINE=InnoDB AUTO_INCREMENT=1000;

-- To avoid issues e.g. on Windows' MySQL Workbench, complaining update violating 
-- 'safe updates' while populating the 'date dimension'
SET SQL_SAFE_UPDATES = 0;

DELIMITER $$

CREATE PROCEDURE datetimeinsert (v_full_date DATE)
BEGIN

    INSERT INTO dim_date (
        fulldate ,
        dayofmonth ,
        dayofyear ,
        dayofweek ,
        dayname ,
        monthnumber,
        monthname,
        year,
        quarter
    ) VALUES (
        v_full_date,
        DAYOFMONTH(v_full_date),
        DAYOFYEAR(v_full_date),
        DAYOFWEEK(v_full_date),
        DAYNAME(v_full_date),
        MONTH(v_full_date),
        MONTHNAME(v_full_date),
        YEAR(v_full_date),
        QUARTER(v_full_date)
    );

END$$

CREATE PROCEDURE datedimbuild (p_start_date DATE, p_end_date DATE)
BEGIN
    DECLARE v_full_date DATE;

    DELETE FROM dim_date;

    SET v_full_date = p_start_date;
    WHILE v_full_date < p_end_date DO
        call datetimeinsert(v_full_date);
        SET v_full_date = DATE_ADD(v_full_date, INTERVAL 1 DAY);
    END WHILE;

    -- moreover, create a 'dummy date' entry to denote 'yet unknown'. This is used e.g.
    -- by orders' shipping_date and paid_date, which may not be knwon at the time of
    -- date extraction
    call datetimeinsert(DATE('9999-12-31'));

END$$

DELIMITER ;

call datedimbuild('2005-01-01', '2007-12-31');
