USE `northwind_dwh`;

CREATE TABLE `staging_orders` (
    id INT NOT NULL,
    
    order_date DATETIME NOT NULL,
    shipped_date DATETIME,
    paid_date DATETIME,
    employee_id INT NOT NULL,
    customer_id INT NOT NULL,

    PRIMARY KEY(id)
);
