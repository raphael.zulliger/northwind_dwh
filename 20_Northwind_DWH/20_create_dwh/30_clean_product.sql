USE `northwind_dwh`;

CREATE TABLE `clean_product` (
    id INT NOT NULL,
    category varchar(50) NOT NULL,
    product_name varchar(50) NOT NULL,
    standard_cost DECIMAL(19,4) NOT NULL,
    PRIMARY KEY(id)
);
